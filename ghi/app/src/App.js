import Nav from "./Nav";
import AttendeeList from "./AttendeesList";
import LocationForm from "./LocationForm";
import ConferenceForm from "./ConferenceForm";
import AttendConference from "./AttendConference"
import PresentationForm from "./PresentationForm";
import { BrowserRouter, Outlet, Route, Routes } from "react-router-dom";
import MainPage from "./MainPage";

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
          <Routes>
            <Route index element={<MainPage />} />
            <Route path="conferences/new" element={<ConferenceForm />} />
            <Route path="attendees/new" element={<AttendConference />} />
            <Route path="locations/new" element={<LocationForm />} />
            <Route path="presentations/new" element={<PresentationForm />} />
            <Route path="attendees" element={<AttendeeList attendees={props.attendees} />} />
          </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
