import React, { useEffect, useState } from "react";

export default function LocationForm() {
  const [formData, setFormData] = useState({})
  const [name, setName] = useState('')
  const [roomCount, setRoomCount] = useState('')
  const [city, setCity] = useState('')
  const [state, setState] = useState('')
  const [states, setStates] = useState([])


  const fetchData = async () => {
    const url = 'http://localhost:8000/api/states/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

      setStates(data.states)


    }
  }

  // useEffect(() => { console.log(states) }, [states])
  useEffect(() => { console.log(name, roomCount, city, state) }, [name, roomCount, city, state])

  useEffect(() => {
    fetchData();
  }, []);

  const handleChange = e => {
    setFormData({ ...formData, [e.target.name]: e.target.value })
  }

  const handleSubmit = async e => {

    e.preventDefault()


    const data = {}

    data.name = name
    data.city = city
    data.state = state
    data.room_count = roomCount
    console.log(data)

    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const res = await fetch(locationUrl, fetchConfig)
    if (res.ok) {
      const newLocation = await res.json()
      console.log(newLocation)

      setName('')
      setRoomCount('')
      setCity('')
      setState('')
    }



  }

  const handleNameChange = e => setName(e.target.value)
  const handleRoomCountChange = e => setRoomCount(e.target.value)
  const handleCityChange = e => setCity(e.target.value)
  const handleStateChange = e => setState(e.target.value)


  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new location</h1>
          <form id="create-location-form" onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <input value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" onChange={handleNameChange} />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={roomCount} placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control" onChange={handleRoomCountChange} />
              <label htmlFor="room_count">Room count</label>
            </div>
            <div className="form-floating mb-3">
              <input value={city} placeholder="City" required type="text" name="city" id="city" className="form-control" onChange={handleCityChange} />
              <label htmlFor="city">City</label>
            </div>
            <div className="mb-3">
              <select value={state} required name="state" id="state" className="form-select" onChange={handleStateChange}>
                <option defaultValue value="">Choose a state</option>
                {states.map(state => <option key={state.abbreviation} value={state.abbreviation}>{state.name}</option>)}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}
